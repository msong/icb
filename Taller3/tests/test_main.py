import unittest
import math
from Taller3.main import parse_float, mean, median, variance, stdev, Line, LineGroup, lines,  main, check_sys_args, PEBKACError, NotEnoughLinesError, UnknownFillMethodError, InvalidTimestampError, LineHasDiffItemLenError


class TestAux(unittest.TestCase):
    def test_mean(self):
        self.assertEqual(mean(7, 11, 99), float(39))
        self.assertTrue(math.isnan(mean()))

    def test_median(self):
        self.assertEqual(median(7, 11, 99), float(11))
        self.assertEqual(median(7, 11, 99, 293), float(55))
        self.assertEqual(median(293, 99, 7, 11), float(55))
        self.assertTrue(math.isnan(median()))

    def test_stdev(self):
        self.assertEqual(stdev(10.0, 20.0), float(5))
        self.assertTrue(math.isnan(stdev()))

    def test_variance(self):
        self.assertTrue(math.isnan(variance()))


class TestLine(unittest.TestCase):
    def test_invalid_timestamp(self):
        with self.assertRaises(InvalidTimestampError):
            Line(1, "3019-24-73T80:17:24", "123.2")


class TestLineGroup(unittest.TestCase):
    def test_default(self):
        l1 = Line(1, "2016-05-03T02:17:24", *"NA,27.77,39.31".split(","))
        l2 = Line(2, "2016-05-03T02:17:26", *"19.13,49.44,NA".split(","))

        group = LineGroup(l1, l2, fill_method="def")
        self.assertEqual(group.csv_format(), "2.0,NA,38.60,NA\n")

    def test_fill_avg(self):
        l1 = Line(1, "2016-05-03T02:17:24", *"NA,27.77,39.31,NA".split(","))
        l2 = Line(2, "2016-05-03T02:17:26", *"19.13,49.44,NA,NA".split(","))

        group = LineGroup(l1, l2, fill_method="prom")
        self.assertEqual(group.csv_format(), "2.0,19.13,38.60,39.31,NA\n")

    def test_fill_median(self):
        l1 = Line(1, "2016-05-03T02:17:24", *"NA,27.77,39.31,NA".split(","))
        l2 = Line(2, "2016-05-03T02:17:26", *"19.13,49.44,NA,NA".split(","))
        l3 = Line(3, "2016-05-03T02:17:36", *"21.13,99.44,NA,NA".split(","))

        group = LineGroup(l1, l2, l3, fill_method="med")
        self.assertEqual(group.csv_format(), "12.0,20.13,58.88,39.31,NA\n")

    def test_fill_dist(self):
        l1 = Line(1, "2016-05-03T02:17:24", *"8,66,NA".split(","))
        l2 = Line(2, "2016-05-03T02:17:26", *"NA,NA,NA".split(","))
        l3 = Line(3, "2016-05-03T02:17:36", *"8,NA,NA".split(","))
        # Si sigma=0, va a rellenar con mu siempre. Algo es algo.
        group = LineGroup(l1, l2, l3, fill_method="dist")
        self.assertEqual(group.csv_format(), "12.0,8.00,66.00,NA\n")

    def test_not_enough_lines(self):
        with self.assertRaises(NotEnoughLinesError):
            LineGroup(fill_method="prom")

    def test_unknown_fill(self):
        l1 = Line(1, "2016-05-03T02:17:24", "22")
        with self.assertRaises(UnknownFillMethodError):
            LineGroup(l1, fill_method="negentropy9000")


class TestLineReader(unittest.TestCase):
    def test_error(self):
        def mock_generator():
            yield "2016-05-03T02:17:26,19.13\n"
            yield "2016-05-03T02:17:26,19.13,666\n"
        with self.assertRaises(LineHasDiffItemLenError):
            list(lines(mock_generator()))


class TestMain(unittest.TestCase):
    def test_main(self):
        input_path = "Taller3/tests/test_input.csv"
        expected_output_path = "Taller3/tests/test_output.csv"
        output_path = "Taller3/tests/output.csv"

        main(["", input_path, output_path, "3"])

        with open(output_path) as output:
            with open(expected_output_path) as expected:
                for expected_line, output_line in zip(expected, output):
                    self.assertEqual(output_line, expected_line)

    def test_main_empty(self):
        input_path = "Taller3/tests/test_input_vacio.csv"
        output_path = "Taller3/tests/output.csv"
        main(["", input_path, output_path, "1"])
        non_empty_lines = 0
        with open(output_path) as output:
            for l in output:
                if l.strip():
                    non_empty_lines += 1

        self.assertEqual(non_empty_lines, 0)

    def test_main_error_exit(self):
        output_path = "Taller3/tests/output.csv"
        broken_input_paths = [
            "Taller3/tests/test_input_len_roto.csv",
            "Taller3/tests/test_input_timestamp_roto.csv",
        ]
        input_path = "Taller3/tests/test_input.csv"

        for path in broken_input_paths:
            with self.assertRaises(SystemExit):
                main(["", path, output_path, 1])
        main(["", input_path, output_path, 1])  # not raises

        with self.assertRaises(SystemExit):
            main([""])


class TestSysArgs(unittest.TestCase):
    def test_bad_args(self):
        bad_args = [
            [""],
            ["", "./tmp/sarasa/espero_que_no_exista", "/tmp/ok.csv", 55],
            ["", "./README.md", "./tmp/sarasa/si_existe/te_zarpas", 55],
            ["", "./README.md", "./tmp/ok.csv", "cincuenta y cinco"],
            ["", "./README.md", "./tmp/ok.csv", -11],
            ["", "./README.md", "./tmp/ok.csv", 55, "negentropy9000"],
        ]
        for args in bad_args:
            with self.assertRaises(PEBKACError):
                check_sys_args(args)
