#!/usr/bin/env python3

import datetime as dt
import math
import random
import os
import sys

# Como se da libertad en el enunciado, mi preferencia personal es declarar
# todos los nombres en ingles, y lo voy a hacer porque creo que mejora la legibilidad.

AVAILABLE_FILL_METHODS = ("def", "prom", "med", "dist")  # cte
# uy, empezamos a los gritos


class ExceptionWithLineNum(Exception):
    def __init__(self, line_num: int):
        super().__init__()
        self.line_num = line_num


class InvalidTimestampError(ExceptionWithLineNum):
    pass


class LineHasDiffItemLenError(ExceptionWithLineNum):
    pass


class UnknownFillMethodError(Exception):
    pass


class NotEnoughLinesError(Exception):
    pass


class PEBKACError(Exception):
    pass


def parse_float(val: str) -> float:
    """Recibe un str y trata de castearlo a float. Si no se puede, devuelve un float
    con el valor especial NaN."""
    try:
        val = float(val.strip())
    except ValueError:
        val = float("nan")
    return val


def no_nan(l: list) -> list:
    """Recibe una lista de floats y devuelve una lista con los floats que no sean NaN."""
    return [x for x in l if not math.isnan(x)]


def mean(*args: float) -> float:
    """Recibe uno o mas floats y devuelve la media. Si *args esta vacio, da NaN."""
    if not args:
        return float("nan")

    return sum(args)/len(args)


def median(*args: float) -> float:
    """Recibe uno o mas floats y devuelve la mediana. Si *args esta vacio, da NaN."""
    if not args:
        return float("nan")

    args_s = sorted(args, key=lambda x: x)
    mid = len(args_s) // 2
    if len(args_s) % 2 == 0:
        return mean(args_s[mid], args_s[mid-1])
    else:
        return args_s[mid]


def variance(*args: float) -> float:
    """Recibe uno o mas floats y devuelve la varianza. Si *args esta vacio, da NaN."""
    if not args:
        return float("nan")

    return mean(*[(x - mean(*args)) ** 2 for x in args])


def stdev(*args: float) -> float:
    """Recibe uno o mas floats y devuelve el desvio estandar. Si *args esta vacio, da NaN."""
    if not args:
        return float("nan")

    return math.sqrt(variance(*args))


class Line():
    """Line representa una linea del archivo.

    Al iterarlo devuelve la medicion de cada sensor.

    Args:
        line_num (int): El numero de linea en el archivo original.
        timestamp (str): Fecha hora en formato ISO-8601.
        *sensors (str): Mediciones de cada sensor.

    Attributes:
        timestamp (datetime): Fecha hora en que se efectuaron las mediciones.

    Raises:
        InvalidTimestampError: Si el timestamp no pudo parsearse.
    """

    def __init__(self, line_num: int, timestamp: str, *sensors: str):
        self._line_num = line_num
        try:
            self.timestamp = dt.datetime.strptime(timestamp.strip(), "%Y-%m-%dT%H:%M:%S")
        except:
            raise InvalidTimestampError(line_num=self._line_num)

        self._sensors = [parse_float(s) for s in sensors]

    def __iter__(self):
        return iter(self._sensors)

    def na_filler(self, filler: iter) -> iter:
        """Generador que da los sensores rellenados con el valor de la posicion correspondiente
        en filler si son NaN."""
        for sensor_value, fill_value in zip(self._sensors, filler):
            yield fill_value if math.isnan(sensor_value) else sensor_value

    def na_filler_dist(self, mus: iter, sigmas: iter) -> iter:
        """Generador que da los sensores rellenados con valores aleatorios
        de una distribucion normal de mu y sigma correspondiente al sensor, si son NaN."""
        for sensor_value, mu, sigma in zip(self._sensors, mus, sigmas):
            yield random.normalvariate(mu, sigma) if math.isnan(sensor_value) else sensor_value


class LineGroup():
    """LineGroup representa un conjunto de :obj:`Line` que fue capturado por la ventana deslizante.

    Args:
        *lines (:obj:`Line`): Lineas individuales que lo componen.
        fill_method (str, optional kwarg): Metodo de rellenado de NaNs.

    Attributes:
        time_range (float): Tiempo en segundos entre la primer y ultima medicion.
        averages (:obj:`list` of float): Lista de promedios de cada uno de los sensores.

    Raises:
        NotEnoughLinesError: Si no hay suficientes lineas.
        UnknownFillMethodError: Si fill_method no se corresponde a uno de los disponibles.
    """

    def __init__(self, *lines: Line, fill_method: str = None):
        if len(lines) < 1:
            raise NotEnoughLinesError()

        self._lines = lines

        if fill_method == "prom":
            filled = self._filler_func(mean)
        elif fill_method == "med":
            filled = self._filler_func(median)
        elif fill_method == "dist":
            filled = self._filler_dist()
        elif fill_method == "def" or not fill_method:
            filled = self._lines
        else:
            raise UnknownFillMethodError()

        self.averages = map(lambda sensor: mean(*sensor), zip(*filled))
        self.time_range = (self._lines[-1].timestamp - self._lines[0].timestamp).total_seconds()

    def _func_no_nans(self, func) -> iter:
        # Calcular func con cada sensor solo con los valores no NaN.
        return map(lambda sensor: func(*no_nan(sensor)), zip(*self._lines))

    def _filler_func(self, func) -> iter:
        # Llamar a na_filler en cada linea y darle un iterador de _func_no_nans(func).
        # Donde func se entiende como mean, median, etc.
        return map(lambda line: line.na_filler(self._func_no_nans(func)), self._lines)

    def _filler_dist(self) -> iter:
        # Lo mismo que antes pero a line.na_filler_dist son distintos parametros.
        return map(lambda line: line.na_filler_dist(self._func_no_nans(mean), self._func_no_nans(stdev)), self._lines)

    def csv_format(self) -> str:
        """Devuelve la representacion del objeto en formato CSV"""
        range_f = format(self.time_range, ".1f")
        averages_f = [format(avg, ".2f") if not math.isnan(avg) else "NA"
                      for avg in self.averages]
        return f"{range_f},{','.join(averages_f)}\n"


def lines(file_obj: iter) -> iter:
    """Generador que toma el iterador en open() y da objetos :obj:`Line`."""
    first_line_len = 0
    for line_num, line_text in enumerate(file_obj, 1):
        if line_text.strip():
            line_items = line_text.split(",")
            if line_num == 1:
                first_line_len = len(line_items)
            elif len(line_items) != first_line_len:
                raise LineHasDiffItemLenError(line_num=line_num)

            yield Line(line_num, *line_items)


def window_groups(line_iter: iter, window_size: int, fill_method=None) -> iter:
    """Generador que implementa la ventana deslizante usando un iterador de lines(), dando :obj:`LineGroup`."""
    window = []
    for line in line_iter:
        window.append(line)
        if len(window) >= window_size:
            yield LineGroup(*window, fill_method=fill_method)
            window.pop(0)


def check_sys_args(sys_args: list) -> (str, str, str, str):
    """Comprueba que los argumentos pasados por sys.argv sean validos."""
    if len(sys_args) < 4:
        raise PEBKACError("Requiere 3 argumentos: main.py <entrada> <salida> <ventana> "
                          f"<opcional: {'/'.join(AVAILABLE_FILL_METHODS)}>")

    _, input_path, output_path, window_size, *extra = sys_args

    fill_method = extra[0] if extra else "def"
    if fill_method and fill_method not in AVAILABLE_FILL_METHODS:
        raise PEBKACError("El metodo de relleno {fill_method} no se corresponde"
                          f"a uno de: {', '.join(AVAILABLE_FILL_METHODS)}")

    if not os.path.exists(input_path):
        raise PEBKACError("El archivo de entrada no existe.")

    has_dir = "/" in output_path or "\\" in output_path
    if has_dir and not os.path.exists(os.path.dirname(output_path)):
        raise PEBKACError("El directorio del archivo de salida no existe.")

    try:
        window_size = int(window_size)
    except ValueError:
        raise PEBKACError("El tamaño de la ventana tiene que ser un numero entero.")

    if window_size < 1:
        raise PEBKACError("El tamaño de la ventana tiene que ser un numero mayor o igual a 1.")

    return input_path, output_path, window_size, fill_method


def main(sys_args: list):
    try:
        input_path, output_path, window_size, fill_method = check_sys_args(sys_args)
    except PEBKACError as e:
        sys.exit(e)

    with open(input_path) as input_file:
        with open(output_path, "w") as output_file:
            lines_iter = lines(input_file)
            window_iter = window_groups(lines_iter, window_size, fill_method=fill_method)

            try:
                for w in window_iter:
                    output_file.write(w.csv_format())
            except LineHasDiffItemLenError as e:
                sys.exit(f"ERROR: La linea {e.line_num} del archivo de entrada tiene "
                         "una cantidad diferente de elementos.")
            except InvalidTimestampError as e:
                sys.exit(f"ERROR: La linea {e.line_num} del archivo de entrada tiene "
                         "un timestamp que no corresponde al formato ISO-8601.")
            finally:
                input_file.close()
                output_file.close()


if __name__ == "__main__":
    main(sys.argv)
