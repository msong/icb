# correr desde Taller3/
import sys
sys.path.append("..")

from Taller3.main import LineGroup, mean, stdev, lines, window_groups
import matplotlib.pyplot as plt


class LineGroupDistAll(LineGroup):
    def __init__(self, mus, sigmas, *args, **kwargs):
        self._mus, self._sigmas = mus, sigmas
        super().__init__(*args, **kwargs)

    def _filler_dist(self) -> iter:
        return map(lambda line: line.na_filler_dist(self._mus, self._sigmas), self._lines)


def window_groups_dist_all(line_iter: iter, window_size: int, mus: list, sigmas: list, fill_method=None) -> iter:
    window = []
    for line in line_iter:
        window.append(line)
        if len(window) >= window_size:
            yield LineGroupDistAll(mus, sigmas, *window, fill_method=fill_method)
            window.pop(0)


if __name__ == "__main__":
    path = "entradas/entrada2.csv"
    file_data = list(open(path))

    window_size = 100
    # con un window_size grande se nota que la complejidad temporal es una mierda

    # Esto es descartable, lo hago rapido y bastante horrible.
    file_cols = [[float(val) for val in col if val != "NA"] for col in zip(*[line.strip().split(",")[1:] for line in file_data])]
    mus = [mean(*s) for s in file_cols]
    sigmas = [stdev(*s) for s in file_cols]
    dist_all = [next(group.averages) for group in list(window_groups_dist_all(lines(iter(file_data)), window_size, mus=mus, sigmas=sigmas, fill_method="dist"))]
    prom = [next(group.averages) for group in list(window_groups(lines(iter(file_data)), window_size, fill_method="prom"))]
    med = [next(group.averages) for group in list(window_groups(lines(iter(file_data)), window_size, fill_method="med"))]
    dist = [next(group.averages) for group in list(window_groups(lines(iter(file_data)), window_size,  fill_method="dist"))]
    default = [next(group.averages) for group in list(window_groups(lines(iter(file_data)), window_size,  fill_method="def"))]

    plt.plot(default, "o-", markersize=5, label="def")
    plt.plot(prom, "x", markersize=5, label="prom")
    plt.plot(med, "+", markersize=5, label="med")
    plt.plot(dist, "s", markersize=5, label="dist")
    plt.plot(dist_all, "o", markersize=5, label="dist_all")
    plt.legend()
    plt.title(f"{path}, window_size = {window_size}")
    plt.show()
