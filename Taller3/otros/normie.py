import scipy.stats
from os import listdir

# Esto lo hago rapido y descartable
input_files = [
    (filename, [
        [float(x) for x in col if x != "NA"]
        for col in zip(*[line.strip().split(",")[1:] for line in open("../entradas/"+filename)])
    ])
    for filename in listdir("../entradas/")
]

for file in input_files:
    filename, file_data = file
    for sensor_num, sensor in enumerate(file_data):
        _, shapiro_p_value = scipy.stats.shapiro(sensor)

        alpha = 0.05
        # Hipotesis nula: es normal
        is_normal = shapiro_p_value > alpha
        is_normal_str = f"no se descarta que sea normal (p={shapiro_p_value:.3f} >{alpha})" if is_normal else f"NO ES NORMAL (p={shapiro_p_value:.3f} <{alpha})"
        print(f"{filename} Sensor #{sensor_num} {is_normal_str}")
