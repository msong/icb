### **El archivo principal y standalone del taller es main.py**. El resto es porque necesito hacer... cosas.

-----

# Rellenando con dist
```
(opcional) Valores de la misma distribucíon de valores. Asi, se debe generar
nuevos valores a partir de la distribucion de todos los valores de esa ventana. 
```
Distribuciones hay muchas. Lo más común en sensores es que los valores sigan una distribución normal.

Pero para no asumir al voleo cual es la distribución que siguen los datos  -*en realidad ya la asumí al voleo, ya está todo hecho y ahora necesito convencerme, pero vamos a seguir con este show culinario*- vamos a hacer la prueba estadística de normalidad [Shapiro-Wilk](https://en.wikipedia.org/wiki/Shapiro%E2%80%93Wilk_test) con `alfa = 0.05` para cada uno de los sensores. La función la podemos encontrar cómodamente envasada en [SciPy](https://docs.scipy.org/doc/scipy-0.19.1/reference/generated/scipy.stats.shapiro.html).

```
$ python3 normie.py

entrada2.csv Sensor #0 no se descarta que sea normal (p=0.158 >0.05)
entrada2.csv Sensor #1 no se descarta que sea normal (p=0.282 >0.05)
entrada2.csv Sensor #2 no se descarta que sea normal (p=0.535 >0.05)
entrada2.csv Sensor #3 no se descarta que sea normal (p=0.962 >0.05)
entrada2.csv Sensor #4 no se descarta que sea normal (p=0.592 >0.05)
entrada2.csv Sensor #5 no se descarta que sea normal (p=0.873 >0.05)
entrada2.csv Sensor #6 no se descarta que sea normal (p=0.324 >0.05)
entrada2.csv Sensor #7 no se descarta que sea normal (p=0.966 >0.05)
entrada2.csv Sensor #8 no se descarta que sea normal (p=0.529 >0.05)
entrada2.csv Sensor #9 no se descarta que sea normal (p=0.500 >0.05)
entrada5.csv Sensor #0 no se descarta que sea normal (p=0.930 >0.05)
entrada3.csv Sensor #0 no se descarta que sea normal (p=0.647 >0.05)
entrada3.csv Sensor #1 no se descarta que sea normal (p=0.288 >0.05)
entrada3.csv Sensor #2 no se descarta que sea normal (p=0.589 >0.05)
entrada3.csv Sensor #3 no se descarta que sea normal (p=0.052 >0.05)
entrada3.csv Sensor #4 no se descarta que sea normal (p=0.190 >0.05)
entrada3.csv Sensor #5 no se descarta que sea normal (p=0.600 >0.05)
entrada3.csv Sensor #6 no se descarta que sea normal (p=0.557 >0.05)
entrada3.csv Sensor #7 no se descarta que sea normal (p=0.847 >0.05)
entrada3.csv Sensor #8 no se descarta que sea normal (p=0.488 >0.05)
entrada3.csv Sensor #9 no se descarta que sea normal (p=0.961 >0.05)
entrada4.csv Sensor #0 no se descarta que sea normal (p=0.410 >0.05)
entrada4.csv Sensor #1 no se descarta que sea normal (p=0.390 >0.05)
entrada4.csv Sensor #2 no se descarta que sea normal (p=0.690 >0.05)
entrada4.csv Sensor #3 no se descarta que sea normal (p=0.324 >0.05)
entrada4.csv Sensor #4 no se descarta que sea normal (p=0.767 >0.05)
entrada4.csv Sensor #5 no se descarta que sea normal (p=0.526 >0.05)
entrada4.csv Sensor #6 NO ES NORMAL (p=0.025 <0.05)
entrada1.csv Sensor #0 no se descarta que sea normal (p=0.231 >0.05)
entrada1.csv Sensor #1 no se descarta que sea normal (p=0.139 >0.05)
entrada1.csv Sensor #2 no se descarta que sea normal (p=0.163 >0.05)
entrada1.csv Sensor #3 no se descarta que sea normal (p=0.959 >0.05)
entrada1.csv Sensor #4 no se descarta que sea normal (p=0.491 >0.05)
```

-*Acá viene esa parte cuando, muy convenientemente y haciendo como que nunca vimos nada, lo barremos abajo de la mesada o ajustamos alfa. No lo voy a hacer*-.

No está mal para asumir normalidad. Otra forma de verlo cualitativamente sería graficar histogramas junto con la campana de Gauss con mu y sigma correspondiente, pero son demasiados sensores.

## Probar que ocurre si se toman todos los valores (la lista completa).
[También va a dar numeritos.](https://pbs.twimg.com/media/DTAtp5sW0AArs65.jpg)

Lo que se pueda llegar a observar va a depender principalmente de una relación entre tamaño de la ventana y cantidad de mediciones. Por ejemplo:

![](otros/Figure_1.svg)

Esto solo es para el primer sensor. La línea esta dada por el método `def`, y donde se ve cortada es que la ventana contiene `NA`. Si no contiene `NA`, como es de esperar los cinco métodos coinciden en el mismo punto, dado que no hay nada que rellenar.
No podemos decir cual método es mejor o peor, pero sí que los puntos para el método `dist_all` tienden a ser más diferentes de los otros, porque rellena los `NA` considerando la distribución de todos los valores y eso tiene mayor dispersión.

Otro ejemplo donde se ve lo mismo:

![](otros/Figure_2.svg)

Sumado a que aparentemente no aporta ninguna ventaja, resulta también un costo extra calcular mu y sigma de antemano para todos los sensores.