## Punto fijo
### Ambos print resuelven la divisíón, pero con una leve  diferencia. Uno es capaz de mantener la precisíón y la otra no. Justifique.

```python
print((fix(10.05) - fix(15.75)) // fix(3.1415))
print(unfix(fix(fix(10.05) - fix(15.75)) // fix(3.1415)))
```
En el primer caso se esta multiplicando todos los valores por 1000, que también luego es el divisor deseado para una precisión de 3 decimales. No se está aplicando `unfix()` al final ni `fix()` al resultado de la resta. Finalmente, lo que se devuelve es solo la parte entera del resultado, equivalente a realizar el cálculo con una precisión de 0 decimales (`divisor=1`).

En el segundo print, el valor obtenido antes de aplicar `unfix()` es `-1815`. Luego, al realizar la división, se obtiene `-1.815`.

Quizás esta resulta una descripción demasiado operativa de lo que está realizando el código Python provisto, pero la idea es que si quiero representar `-1.815` en punto fijo, esencialmente estoy almacenando un entero `-1815` junto con un factor también entero que lo que está indicando es donde ubicar la coma.

### Modificar la precisión a 5 decimales y codificar los siguientes  números y cálculos en notación fija

```python
divisor = 100000
print(unfix(fix(0.00000456)))
print(0.00000456)
# 0.0
# 4.56e-06

print(unfix(fix(9223372036854775807)))
print(9223372036854775807)
# 9.223372036854776e+18
# 9223372036854775807

print(unfix(unfix(fix(43.001) * fix(0.00751))))
print(43.001 * 0.00751)
# 0.32293751
# 0.32293751

print(unfix(unfix(fix(0.00000001) * fix(1.4142135623730951))))
print(0.00000001 * 1.4142135623730951)
# 0.0
# 1.4142135623730952e-08

print(unfix(fix(0.1) + fix(0.1) + fix(0.1) - fix(0.3)))
print(0.1 + 0.1 + 0.1 - 0.3)
# 0.0
# 5.551115123125783e-17
```

Una de las ventajas de punto fijo es que se pueden representar numeros que en punto flotante su representacion no seria exacta. Esto es por ejemplo el caso de hacer `0.1 + 0.1 + 0.1 - 0.3` que como se ve mas adelante, el resultado no es exactamente 0. Esto en escenarios reales puede terminar acumulando error si seguimos haciendo operaciones, y ocasionando problemas al comparar numeros.
Otra de las ventajas es la performance, ya que los numeros se almacenan en memoria como enteros y existen mas optimizaciones en la CPU para operar con estos que con numeros en punto flotante.
Como desventaja, se puede mencionar que se posee un rango menor de valores a representar y que la cantidad de decimales debe definirse a priori.


### Estándar IEEE-754
```python
def verBits(n):
    return "".join(["{:08b}".format(c) for c in pack("!f", n)])

print(verBits(1))
# 00111111100000000000000000000000
```

### 1. ¿Qué valor debería ser la mantisa? No se olvide que está normalizada.
Si esta normalizada, la mantisa deberia tener el valor 1, pero no se incluye en la representación binaria y son todos ceros.

### 2. Conociendo la mantisa, ¿qué valor debería tener el exponente?
127 en binario.


### 3. Identifique los bits del exponente.
```
0  01111111  00000000000000000000000
   ========
   Exponente
```

### 4. Realice el mismo experimento con el valor −1. 5. Identifique el bit de signo.
```
1       01111111     00000000000000000000000
=       ========     =======================
Signo   Exponente    Mantisa
```

### 6. Identifique el cambio de signo del exponente usando el valor 0,5.
```
0       01111110     00000000000000000000000
=       ========     =======================
Signo   Exponente    Mantisa
```

### 7. Diagrame la organización de los bits e indique a que formato IEEE-754 corresponde.

```
0       01111110     00000000000000000000000
=       ========     =======================
1 bit   8 bits       23 bits                = 32 bits
```
Punto flotante de precisión simple, 32 bits.

### 8. Pruebe los siguientes valores y registre como se codifican: `float('Nan')`, `float('inf')`, `float('-inf')`

```
NaN
0       11111111     10000000000000000000000
=       ========     =======================
Signo   Exponente    Mantisa

+Inf
0       11111111     00000000000000000000000
=       ========     =======================
Signo   Exponente    Mantisa

-Inf
1       11111111     00000000000000000000000
=       ========     =======================
Signo   Exponente    Mantisa
```

## Programación de pila
### Muestre una forma de modificar el codigo para que sume 5 a 150, limitandose a las instrucciones ya usadas.
```c
int main(void)
{
    double y;
    const double k = 150.0;
    const double j = 5.0;

    __asm__ ("fldl %1;"
    "fldl %2;"
    "faddp;"
    "fstl %0;" : "=m" (y) : "m" (k), "m" (j));

    printf("%e\n", y);
    // 1.550000e+02

    return 0;
}
```

### Ahora, codifique las siguientes operaciones
```c
int main(void)
{
    double y;

    double a = 43.001;
    double b = 0.00751;
    __asm__ ("fldl %1;"
    "fldl %2;"
    "fmulp;"
    "fstl %0;" : "=m" (y) : "m" (a), "m" (b));
    printf("%e\n", y);
    // 3.229375e-01

    a = 0.00000001;
    b = 1.4142135623730951;
    __asm__ ("fldl %1;"
    "fldl %2;"
    "fmulp;"
    "fstl %0;" : "=m" (y) : "m" (a), "m" (b));
    printf("%e\n", y);
    // 1.414214e-08

    a = 0.1;
    b = 0.3;
    __asm__ ("fldl %1;"
    "fldl %1;"
    "faddp;"
    "fldl %1;"
    "faddp;"
    "fldl %2;"
    "fsubp;"
    "fstl %0;" : "=m" (y) : "m" (a), "m" (b));
    printf("%e\n", y);
    // -2.775558e-17

    return 0;
}
```
### ¿Puede observar el mismo comportamiento que en los ejercicios anteriores?

```
1) 43.001 * 0.00751
Python float: 0.32293751
ASM FPU:      3.229375e-01

2) 0.00000001 * 1.4142135623730951
Python float: 1.4142135623730952e-08
ASM FPU:      1.414214e-08

3) 0.1 + 0.1 + 0.1 − 0.3
Python float: 5.551115123125783e-17
ASM FPU:      -2.775558e-17
```

Tanto el tipo double de C como float en Python utilizan IEEE-754 binary64. El caso 1 y 2 resultan comparables, solo si consideramos que el resultado es muy cercano a 0. Respecto a la diferencia observada con el resultado, esto puede deberse a diferencias en la implementacion de como es realizado el calculo internamente en Python.