from struct import pack

divisor = 1000


def fix(val):
    return int(val * divisor)


def unfix(val):
    return float(val) / divisor


print(type((fix(10.05) - fix(15.75)) // fix(3.1415)))
print(type(unfix(fix(fix(10.05) - fix(15.75)) // fix(3.1415))))

print("========")

divisor = 100000
print(unfix(fix(0.00000456)))
print(0.00000456)
print()
print(unfix(fix(9223372036854775807)))
print(9223372036854775807)
print()
print(unfix(unfix(fix(43.001) * fix(0.00751))))
print(43.001 * 0.00751)
print()
print(unfix(unfix(fix(0.00000001) * fix(1.4142135623730951))))
print(0.00000001 * 1.4142135623730951)
print()
print(unfix(fix(0.1) + fix(0.1) + fix(0.1) - fix(0.3)))
print(0.1 + 0.1 + 0.1 - 0.3)

print("========")


def verBits(n):
    return "".join(["{:08b}".format(c) for c in pack("!f", n)])


print(verBits(1))
print(verBits(0.5))
print(verBits(float("nan")))
print(verBits(float("inf")))
print(verBits(float("-inf")))
