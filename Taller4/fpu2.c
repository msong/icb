#include <stdio.h>

int main(void)
{
    double y;

    double a = 43.001;
    double b = 0.00751;
    __asm__ ("fldl 0.5;"
    "fldl %2;"
    "fmulp;"
    "fstl %0;" : "=m" (y) : "m" (a), "m" (b));
    printf("%e\n", y);
    // 3.229375e-01

    a = 0.00000001;
    b = 1.4142135623730951;
    __asm__ ("fldl %1;"
    "fldl %2;"
    "fmulp;"
    "fstl %0;" : "=m" (y) : "m" (a), "m" (b));
    printf("%e\n", y);
    // 1.414214e-08

    a = 0.1;
    b = 0.3;
    __asm__ ("fldl %1;"
    "fldl %1;"
    "faddp;"
    "fldl %1;"
    "faddp;"
    "fldl %2;"
    "fsubp;"
    "fstl %0;" : "=m" (y) : "m" (a), "m" (b));
    printf("%e\n", y);
    // -2.775558e-17

    a = 0.1;
    b = -0.3;
    __asm__ ("fldl %2;"
    "fldl %1;"
    "faddp;"
    "fldl %1;"
    "faddp;"
    "fldl %1;"
    "faddp;"
    "fstl %0;" : "=m" (y) : "m" (a), "m" (b));
    printf("%e\n", y);

    return 0;
}