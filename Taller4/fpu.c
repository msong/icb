#include <stdio.h>

int main(void)
{
    double y;
    const double k = 150.0;
    const double j = 5.0;

    __asm__ ("fldl %1;"
    "fldl %2;"
    "faddp;"
    "fstl %0;" : "=m" (y) : "m" (k), "m" (j));

    printf("%e\n", y);
    // 1.550000e+02

    return 0;
}