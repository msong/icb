import unittest
from Taller2.main import hayBorde, esBorde, listaTriangular


class TestHayBorde(unittest.TestCase):
    def test_con_borde(self):
        self.assertTrue(hayBorde([1, 2, 2, 3, 3, 5], 2, 1))
        self.assertTrue(hayBorde([1, 2, 2, 2, 8, 8, 8], 3, 6))
        self.assertTrue(hayBorde([1, 2], 1, 1))
        self.assertTrue(hayBorde([1, 2, 3, 2, 1, 1, 1, 1, 9, 9, 9, 9, 0], 4, 8))
        self.assertTrue(hayBorde([3, 3, 2, 2], 1, 1))

    def test_sin_borde(self):
        self.assertFalse(hayBorde([1, 2, 3, 4, 5], 2, 1))
        self.assertFalse(hayBorde([1, 2], 99, 182))

    def test_edges(self):
        self.assertTrue(hayBorde([1, 0, 0, 0, 0], 2, 0))
        self.assertFalse(hayBorde([], 1, 1))
        self.assertFalse(hayBorde([1], 1, 1))

    def test_negativos(self):
        self.assertTrue(hayBorde([-8, -1, -1, -5, -5], 2, 4))
        self.assertFalse(hayBorde([-8, -1, -2, -5, -5], 2, 4))

    def test_raise(self):
        with self.assertRaises(ValueError):
            hayBorde([], 0, 0)


class TestEsBorde(unittest.TestCase):
    def test_borde(self):
        self.assertTrue(esBorde([6, 6, 8, 8]))
        self.assertTrue(esBorde([1, 2]))
        self.assertTrue(esBorde([-1, -1, -5, -5]))
        self.assertTrue(esBorde([8, 8, -2, -2]))

    def test_no_borde(self):
        self.assertFalse(esBorde([1, 1, 2]))
        self.assertFalse(esBorde([1, 2, 1]))
        self.assertFalse(esBorde([1, 2, 1, 2]))
        self.assertFalse(esBorde([1, 1, 1, 1, 2, 2]))
        self.assertFalse(esBorde([-2, 2, 1, -1]))


class TestTriangular(unittest.TestCase):
    def test_triangular(self):
        self.assertTrue(listaTriangular([10, 20, 10]))
        self.assertTrue(listaTriangular([1, 2, 3, 4, 3, 2]))
        self.assertTrue(listaTriangular([8, 9, 1]))

    def test_no_triangular(self):
        self.assertFalse(listaTriangular([9, 8, 9]))
        self.assertFalse(listaTriangular([87, 56, 23, 48]))

    def test_especiales(self):
        self.assertTrue(listaTriangular([1, 1, 1]))
        self.assertTrue(listaTriangular([1, 1, 1, 0]))
        self.assertTrue(listaTriangular([1, 2, 3]))
        self.assertTrue(listaTriangular([3, 2, 1]))
        self.assertTrue(listaTriangular([]))

    def test_negativos(self):
        self.assertTrue(listaTriangular([-55, 10, -1]))
        self.assertFalse(listaTriangular([-180, -250, -1]))
