def esBorde(l):
    """
    esBorde recibe una lista de enteros l y devuelve True si es de la forma [x,x,x...,y,y,y...],
    de lo contrario False.
    """
    if len(l) % 2 != 0:
        return False

    b = True
    i = 0
    while b and i < len(l) // 2:
        if l[0] != l[i] or l[-1] != l[len(l)-i-1]:
            b = False
        i += 1
    return b


def hayBorde(l, n, h):
    """
    hayBorde recibe una lista de enteros l, y devuelve True si existe un borde de longitud n
    y altura (absoluta) h, False si no lo hay.
    """
    if n < 1:
        raise ValueError("El parámetro n tiene que ser >= 1")
        # Una ventana de tamaño 0 o negativo no tiene sentido.

    found = False
    i = 0
    while not found and i < len(l)-2*n+1:
        window = l[i:i+2*n]
        if esBorde(window):
            if abs(window[-1] - window[0]) == h:
                found = True
        i += 1
    return found


def listaTriangular(l):
    """
    listaTriangular recibe una lista de enteros l y devuelve True si es triangular, False si no lo es.
    """

    pivot = False
    t = True
    i = 1
    while t and i < len(l):
        if not pivot:
            if l[i] < l[i-1]:
                # Aca decrece.
                pivot = True
        else:
            if l[i] > l[i-1]:
                # Estaba decreciendo y ahora vuelve a crecer.
                # Ya no es triangular.
                t = False
        i += 1
    return t
