# Talleres Flash ICB
[![pipeline status](https://gitlab.com/msong/icb/badges/master/pipeline.svg)](https://gitlab.com/msong/icb/pipelines/latest)
[![coverage report](https://gitlab.com/msong/icb/badges/master/coverage.svg)](https://gitlab.com/msong/icb/pipelines/latest)

El entrypoint para cada uno de los talleres es `TallerN/main.py`.
La version utilizada es Python >= 3.6.

---

No es necesario instalar los packages que estan en `requirements.txt`, solo contiene herramientas que uso y sus dependencias (autopep8, pytest, etc).

```
$ virtualenv venv/
$ source venv/bin/activate
$ pip install -r requirements.txt
```

Para correr tests, desde el directorio raiz: `python -m unittest` o `pytest`