import re
import subprocess
import statistics
import os

RESULTS_DIR = "resultados/"


class Benchmark:
    def __init__(self, name, matrix_size_runs, source_file, build_cmd, run_cmd):
        self.name = name
        self.matrix_size_runs = matrix_size_runs
        self.source_file = source_file
        self.build_cmd = build_cmd
        self.run_cmd = run_cmd

    def replace_n(self, n_matrix):
        with open(self.source_file) as f:
            replaced = re.sub(r"(\d+)(.*NLINE)", str(n_matrix) + r"\2", f.read())
        with open(self.source_file, "w") as fw:
            fw.write(replaced)

    def build(self):
        r = subprocess.run(self.build_cmd.split(" "), stdout=subprocess.PIPE)
        print(f"[BUILD] {str(r)}")

    def run(self):
        r = subprocess.run(self.run_cmd.split(" "), stdout=subprocess.PIPE)
        return r.stdout

    def start(self):
        for n_matrix, n_runs in self.matrix_size_runs:
            self.replace_n(n_matrix)
            if self.build_cmd:
                self.build()

            dir = f"{RESULTS_DIR}/{self.name}"
            if not os.path.exists(dir):
                os.makedirs(dir)

            with open(f"{dir}/{n_matrix}.csv", "w") as fw:
                times = []
                for i in range(1, n_runs + 1):
                    elapsed = float(self.run().strip())
                    print(f"[RUN {100*i/n_runs:.2f}%][{self.name} N={n_matrix}] \t {elapsed}")
                    times.append(elapsed)
                fw.write(f"{statistics.mean(times)}, {statistics.stdev(times)}\n")
                fw.write(", ".join(str(x) for x in times))


if __name__ == "__main__":
    # (tamano matriz, cantidad runs)
    matrix_size_runs = [(50, 1000), (100, 1000), (500, 500), (1000, 10), (2000, 3)]
    # matrix_size_runs = [(50, 1), (100, 1), (500, 1), (1000, 1), (2000, 1)]

    Benchmark("Python NumPy", matrix_size_runs, "code/python/main.py", "", "python3 code/python/main.py numpy").start()
    Benchmark("Python Theano", matrix_size_runs, "code/python-theano/main.py", "", "python3 code/python-theano/main.py").start()
    Benchmark("C++ -O3", matrix_size_runs, "code/cpp/main.cpp", "g++ code/cpp/main.cpp -O3 -o code/cpp/main", "code/cpp/main").start()
    Benchmark("C++ -O1", matrix_size_runs, "code/cpp/main.cpp", "g++ code/cpp/main.cpp -O1 -o code/cpp/main", "code/cpp/main").start()
    Benchmark("Go", matrix_size_runs, "code/go/main.go", "go build -o code/go/main code/go/main.go", "code/go/main").start()
    Benchmark("Rust", matrix_size_runs, "code/rust/src/main.rs", "cargo build --manifest-path=code/rust/Cargo.toml --release", "code/rust/target/release/rust").start()
    Benchmark("JavaScript", matrix_size_runs, "code/js/main.js", "", "node code/js/main.js").start()
    Benchmark("Python", matrix_size_runs, "code/python/main.py", "", "python3 code/python/main.py listas").start()
    Benchmark("Lua", matrix_size_runs, "code/lua/main.lua", "", "lua code/lua/main.lua").start()
