import numpy as np
import matplotlib.pyplot as plt
import os
from color import color

results_dir = "../resultados/"
exclude = []  # ["Python", "Lua"]
log_scale = True

sizes = (50, 100, 500, 1000, 2000)
benchmarks = []

for bench_name in os.listdir(results_dir):
    times = []
    for filename in os.listdir(results_dir + bench_name):
        size = int(filename.split(".")[0])
        mean_str, stdev_str = open(results_dir + f"{bench_name}/{filename}").readline().split(",")
        mean, stdev = float(mean_str), float(stdev_str)
        times.append((size, mean, stdev))
    times.sort(key=lambda x: x[0])
    benchmarks.append([bench_name, times])

benchmarks.sort(key=lambda x: x[1][-1][1])

fig, ax = plt.subplots()
bars = []

ind = np.arange(len(sizes))
width = 0.03
for i, v in enumerate(benchmarks):
    name, times = v
    if name not in exclude:
        sizes, means, stdevs = list(zip(*times))
        b = ax.bar(ind + (i * width), means, width, align="center", bottom=0, yerr=stdevs, capsize=3, color=color(name))
        bars.append(b)

ax.legend(bars, [x[0] for x in benchmarks])

ax.set_xticks(ind + width / 2)
ax.set_xticklabels(sizes)

ax.autoscale_view()

plt.xlabel("N")
plt.ylabel("segs")
if log_scale:
    plt.yscale("log")
    plt.ylabel("log(segs)")
ax.yaxis.grid(True, which="both", zorder=0, linestyle="dotted")
ax.set_axisbelow(True)
plt.show()
