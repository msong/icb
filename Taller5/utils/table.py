# Generar la tabla de doble entrada Markdown con lenguaje media +/- stdev (CV).
# Es mas rapido que copypastear a manopla.

import os
import decimal

header = "| LangN | 50  | 100 | 500 | 1000 | 2000 |"
sep = "| ------ | --- | --- | --- | ---- | ---- |"

results_dir = "../resultados/"
benchmarks = []

for bench_name in os.listdir(results_dir):
    times = []
    for filename in os.listdir(results_dir + bench_name):
        size = int(filename.split(".")[0])
        mean_str, stdev_str = open(results_dir + f"{bench_name}/{filename}").readline().split(",")
        mean, stdev = float(mean_str), float(stdev_str)
        times.append((size, mean, stdev))
    times.sort(key=lambda x: x[0])
    benchmarks.append([bench_name, times])
benchmarks.sort(key=lambda x: x[1][-1][1])


print(header)
print(sep)
for name, res in benchmarks:
    print(name + " | ", end="")
    for size_times in res:
        size, mean, stdev = size_times
        stdev_d = decimal.Decimal(stdev)
        mean_d = decimal.Decimal(mean)

        if stdev_d.adjusted() < 0:
            # Stdev con 1 cifra significativa
            stdev_str = ("%." + str(stdev_d.adjusted() * -1) + "f") % stdev_d
            # Redondear mean en base a las cifras de stdev
            mean_str = ("%." + str(stdev_d.adjusted() * -1) + "f") % mean_d
        else:
            stdev_str = ("%.0f") % stdev_d
            mean_str = ("%.0f") % mean_d

        print(f"{mean_str} ± {stdev_str} ({100*stdev/mean:.1f}%) | ", end="")
    print("")
