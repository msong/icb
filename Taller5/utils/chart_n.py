import matplotlib.pyplot as plt
import os
import numpy as np
from color import color
import statistics

files = ["Python.csv", "JavaScript.csv", "Rust.csv", "C++ -O3.csv", "Python NumPy.csv", "Go.csv"]
results_dir = "../t_vs_n/"

fig, ax = plt.subplots()
for file in files:
    name = file.split(".")[0]
    color_hex = color(name)
    f = [x.split(",") for x in [line for line in open(results_dir + file, "r").read().split("\n") if line]][100:]
    x, y = [int(x[0]) for x in f if x], [float(x[1]) for x in f if x]
    ax.plot(x, y, color=color_hex, marker="o", ls="", markersize=3, label=name)

    x_ls = np.linspace(200, 500, 100)
    y0 = min(y[1:10])
    a = statistics.mean(y[1:10]) / (200**3)
    o_y = a * (x_ls)**3

    ax.plot(x_ls, o_y, color="black", ls="-", linewidth=3)
    ax.plot(x_ls, o_y, color=color_hex, ls=":", linewidth=2)

ax.autoscale_view()
plt.yscale("log")
plt.legend()
plt.xlabel("N")
plt.ylabel("log(segs)")

plt.show()
