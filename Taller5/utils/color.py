import requests
import pylev
import json

colors = json.loads(requests.get("https://raw.githubusercontent.com/ozh/github-colors/master/colors.json").text)
colors["python numpy"] = {"color": "#f59e42"}
colors["python theano"] = {"color": "#f02f22"}


def color(text):
    min_dist = len(text)
    min_dist_color = ""
    for name, val in colors.items():
        dist = pylev.levenshtein(name.lower(), text.lower())
        if dist < min_dist:
            min_dist = dist
            min_dist_color = val["color"]
    return min_dist_color
