# Taller 5
# Estructura del taller
- `entrega/` contiene symlinks a los archivos pedidos en Python y C++, que estan ubicados en `code/python/main.py` y `code/cpp/main.cpp` respectivamente.
- `resultados/` contiene carpetas para cada lenguaje y archivos CSV para cada N. Los archivos CSV tienen en la primera línea la media y desviación estándar, en la segunda línea los tiempos de cada una de las corridas individuales.
- `utils/` contiene scripts para generar los gráficos y tablas a partir de los resultados.

# Metodología
Se van a probar los siguientes lenguajes, librerias y opciones de compilador, entre lo que fue solicitado y otros bonus tracks:

- Python
- Python NumPy (OpenBLAS)
- C++ (g++ -O1)
- C++ (g++ -O3)
- Lua
- Go (gc)
- Rust (rustc --release, unsafe sin bounds check)
- JavaScript (V8 engine Node.js/Chromium)
- Python Theano (OpenCL, clBLAS)

Los resultados van a ser informados en segundos como `media ± stdev (CV%)`, donde `CV%` es el coeficiente de variación `100*stdev/media`. Se realizaron la siguiente cantidad de corridas para los respectivos tamaños de matriz:

| NxN * NxN, N= | runs |
| ------------- | ---- |
| 50            | 1000 |
| 100           | 1000 |
| 500           | 500  |
| 1000          | 10   |
| 2000          | 3    |


Para facilitar las cosas hice el script `benchmark.py`, que para cada N se ocupa de:
1. Abrir el archivo fuente, matchear el regex `(\d+)(.*NLINE)`, reemplazarlo por el N a probar.
2. Correr el comando de build, si lo hubiese.
3. Correr el comando especificado x veces, que espera que printee solamente el tiempo que le tomó. Guardarlos.
4. En `resultados/<N>/<name>.csv` guardar en la primera linea `media, stdev`, y las mediciones individuales en la segunda línea.

Para la ejecución se optó por deshabilitar `X11` para no tener programas extra del escritorio corriendo que puedan agregar dispersión a los resultados, luego desde `tty1` fue cuestión de ejecutar `python benchmark.py && python benchmark_n.py` un sábado a la tarde, al no tener nada que hacer obligándome a salir a tomar algo. Que taller horrible.


## Software
Arch Linux, todos los interpretes, compiladores y librerias son las disponibles en el repositorio oficial al dia de la fecha. Excepto por las dos subsecciones que se explican a continuación.


### San Blas! NumPy se atragantó
Lo primero que me encontré fue que la implementación de lista de listas en Python puro obtenia un mejor tiempo que NumPy. Cuando hago algo que funciona demasiado bien para ser verdad desconfío, entonces me puse a ver cual podía ser el problema.


No se como será en otras distros o Anaconda, pero Arch instala por default como dependencia de NumPy [esta implementación de BLAS](http://www.netlib.org/lapack/), y anda bastante mal.

Viendo https://markus-beuckelmann.de/blog/boosting-numpy-blas.html donde hay algunos benchmarks comparativos, decidí compilar NumPy con OpenBLAS, y ahí si vuela...

### A 100 me duermo. Tranqui 120.
Que aparatitos nobles que son las GPU. Artefactos mejorados por el ocio, facilitadores de cálculos trascendentales, de [nuevos paradigmas monetarios](https://elsol-compress.s3-accelerate.amazonaws.com/files/1522677442224LAMBORGHINI%20BITCOIN%20COVER.jpg) y del [handshake WPA de tu vecino](https://pbs.twimg.com/media/EHu5KgrXYAADAgM.jpg). Mundo de los [sencillos hello world de 133 lineas](https://gist.github.com/ddemidov/2925717) y de los vendor lock-in.

Como siempre, [un placer](https://imgs.xkcd.com/comics/x11.png) instalar drivers propietarios de GPU en Linux. Yo pensaba que el equipo verde es medio ortiva, pero a AMD le encanta empezar cosas y [dejarlas a medio andar](https://aur.archlinux.org/packages/rocr-runtime/).

Para sacar Theano andando con GPU, utilicé las siguientes dependencias de las catacumbas de AUR: [opencl-amd](https://aur.archlinux.org/packages/opencl-amd/), [clblas](https://aur.archlinux.org/packages/clblas/), [libgpuarray-git](https://aur.archlinux.org/packages/libgpuarray-git/).

## Hardware
- AMD Ryzen 7 2700X 4.2 GHz
- 16 GB Gskill TridentZ DDR4 3200 MHz
- AMD RX 480 8 GB


# Resultados

| LangN         | 50                         | 100                       | 500                     | 1000                   | 2000                 |
| ------------- | -------------------------- | ------------------------- | ----------------------- | ---------------------- | -------------------- |
| Python Theano | 0.00041 ± 0.00007 (17.0%)  | 0.00061 ± 0.00010 (16.1%) | 0.0040 ± 0.0002 (6.1%)  | 0.0112 ± 0.0005 (4.4%) | 0.055 ± 0.001 (2.6%) |
| Python NumPy  | 0.00006 ± 0.00005 (89.0%)  | 0.00013 ± 0.00001 (8.8%)  | 0.0062 ± 0.0008 (12.9%) | 0.021 ± 0.004 (19.4%)  | 0.16 ± 0.03 (17.1%)  |
| C++ -O3       | 0.000040 ± 0.000003 (7.2%) | 0.00032 ± 0.00002 (6.1%)  | 0.04 ± 0.01 (25.8%)     | 0.191 ± 0.005 (2.8%)   | 4.8 ± 0.3 (6.5%)     |
| Rust          | 0.00009 ± 0.00005 (50.8%)  | 0.00075 ± 0.00003 (3.7%)  | 0.071 ± 0.004 (5.6%)    | 0.602 ± 0.005 (0.8%)   | 13 ± 4 (32.4%)       |
| C++ -O1       | 0.00042 ± 0.00005 (12.1%)  | 0.0043 ± 0.0002 (5.5%)    | 0.330 ± 0.006 (1.7%)    | 2.532 ± 0.001 (0.0%)   | 21.41 ± 0.05 (0.3%)  |
| Go            | 0.00053 ± 0.00008 (14.7%)  | 0.0046 ± 0.0006 (13.6%)   | 0.331 ± 0.006 (1.7%)    | 2.652 ± 0.007 (0.3%)   | 25.61 ± 0.09 (0.3%)  |
| JavaScript    | 0.0074 ± 0.0010 (13.5%)    | 0.011 ± 0.001 (12.7%)     | 0.359 ± 0.002 (0.5%)    | 3.21 ± 0.04 (1.1%)     | 34.9 ± 0.5 (1.5%)    |
| Lua           | 0.017 ± 0.001 (5.9%)       | 0.086 ± 0.003 (3.8%)      | 8.2 ± 0.2 (2.2%)        | 72.3 ± 0.9 (1.3%)      | 603 ± 3 (0.6%)       |
| Python        | 0.0085 ± 0.0002 (2.4%)     | 0.062 ± 0.001 (2.2%)      | 7.1 ± 0.2 (2.5%)        | 67 ± 1 (2.2%)          | 604 ± 24 (3.9%)      |

En hoja aparte se incluye el gráfico de barras (`img/Figure_1.svg`) en escala logarítmica, ya que los tiempos son muy distintos entre sí.

### Conclusiones
Tanto NumPy como Theano poseen tiempos bastante altos si el valor de N es comparativamente bajo. En el caso de Theano, tiene que convertir los arrays de NumPy de algún modo y mandarlos a la GPU, y viceversa para obtener el resultado, tiempo que para matrices grandes es despreciable frente a lo que tarda el cálculo.
Lo mismo pasa en NumPy, ya que está escrito en C, OpenBLAS en Fortran, por lo cual hay conversiones extras desde el tipo lista nativo de Python.

Sobre Python, probé también de representar cada matriz en una única lista, pero la performance era horrenda y no lo incluí. Aparentemente le resulta más económico acceder a varias listas que la aritmética para los índices.

La performance de Go es muy parecida a la de C++ compilado con g++ -O1. Demasiado parecida. Estaría bueno que se copien de las optimizaciones que hace g++ -O3 también.

Sobre Lua, la sintaxis ya me parecía horrible, para peor los índices empiezan en 1, y también resulta que la performance es similar a Python sin las bondades de expresividad de éste. De todos modos su target tampoco son los cálculos pesados si no las aplicaciones embebidas, pero eso suena demasiado serio. Digamos la verdad, para lo único que se usa es para memes hechos código en forma de mods.

De Rust no se que decir, esta fue mi primera impresión y no tengo mucho para decir. Es como un C++ pero con profilaxis por todos lados. Te grita si no te lavaste las manos, te hace sentir culpable escribiendo `unsafe`.

Si bien el código JavaScript fue ejecutado en Node.js, es el mismo motor que usa Chromium y los tiempos eran bastante comparables copypasteando el código en la consola del browser. \
La verdad me sorprendió, esos tiempos no me los esperaba para nada, se nota que hay plata. Que bueno está poder correr todos los trackers de telemetria y publicidad que te meten con una buena performance.

No puedo decir lo mismo de Firefox, que lo probé pero no lo incluí porque el tiempo que tardaba era una verguenza, peor que Python o Lua.


## Tiempo en función de N
Se dice por ahí que la multiplicacion de matrices naive tiene una complejidad temporal `O(n^3)` en el peor caso.

Para contrastar los datos experimentales con lo esperado, voy a considerar que el tiempo que tarda cumple una función de la forma `t = a*N^3`, donde `N` es de la matriz y `a` es algún factor. Podría hacer un ajuste con `scipy` u otras librerias, pero no me interesa obtener la mejor curva con la mínima distancia a los puntos, si no como sólo considerando algunos primeros valores de `N` ver si la curva sigue siendo similar a los puntos en todo el rango o se va alejando.

Si arrancamos con `N=100`, y `t0` es el tiempo que tarda:

```math
t_0 = a*100^{3}
```

Entonces:

```math
a = \frac{t0}{100^3}
```

Concretamente, para `t0` voy a utilizar el promedio de los tiempos para los primeros 10 `N`.
Esto es lo que se obtiene para C++ -O3, con la línea punteada siendo la mencionada función:

![](img/tvsn-cpp.png)

Parece cumplirse bastante bien.

Ahora, para algunos más con N entre 200 y 500, en escala logaritmica:

![](img/Figure_2.svg)

Python y C++ parecen seguirlo al pie de la letra. Para NumPy no, ya que se observa que la pendiente disminuye comparada a la función punteada, y es de esperar porque hay algoritmos levementes mejores que `O(n^3)`. También como curiosidad se observa un salto a `N=256`, número sospechosamente redondo, que al repetir la prueba ocurre siempre, quizás algún tipo de optimización.


Ahora, el motivo por el que incluí este apartado: Rust, JavaScript y Go. En el caso de Rust es más obvio, al principio cumple, luego se desvía. Asumiendo que no haya falencias en esta forma de compararlos, tampoco me atrevo a decir que las optimizaciones de un compilador puedan disminuir la complejidad temporal de un algoritmo. ¿Qué onda esta magia negra?