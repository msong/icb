/*
$ go build main.go
*/

package main

import (
	"fmt"
	"math/rand"
	"time"
)

const N = 5000 // NLINE
const S = N * N

func matrizCeros() [S]float64 {
	var a [S]float64
	return a
}

func matrizAleatoria(a *[S]float64) {
	for i := range a {
		a[i] = rand.Float64()
	}
}

func multMatrices(a *[S]float64, b *[S]float64, res *[S]float64) {
	for i := 0; i < N; i++ {
		for j := 0; j < N; j++ {
			for k := 0; k < N; k++ {
				res[i*N+j] += a[i*N+k] * b[k*N+j]
			}
		}
	}
}

func medirTiempos(fn func(a *[S]float64, b *[S]float64, res *[S]float64), a *[S]float64, b *[S]float64, res *[S]float64) {
	start := time.Now()
	fn(a, b, res)
	fmt.Println(time.Since(start).Seconds())
}

func realizarExperimento() {
	a := matrizCeros()
	b := matrizCeros()
	c := matrizCeros()
	matrizAleatoria(&a)
	matrizAleatoria(&b)

	medirTiempos(multMatrices, &a, &b, &c)
}

func main() {
	realizarExperimento()
}
