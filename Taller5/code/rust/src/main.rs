/*
$ cargo build --release
$ ./target/release/rust
*/

extern crate rand;

use rand::Rng;
use std::time::Instant;

const N: usize = 5000; // NLINE
const S: usize = N*N;

fn matriz_ceros() -> Vec<f64> {
    return vec![0.0; S];
}

fn matriz_aleatoria(a: &mut Vec<f64>) {
    let mut rng = rand::thread_rng();

    for i in 0..N*N {
            a[i] = rng.gen();
    }
}

fn mult_matrices(a: &Vec<f64>, b: &Vec<f64>, res: &mut Vec<f64>) {
	for i in 0..N {
		for j in 0..N {
			for k in 0..N {
                // https://doc.rust-lang.org/std/primitive.slice.html#method.get_unchecked_mut
                unsafe {
                    *res.get_unchecked_mut(i*N+j) = a.get_unchecked(i*N+k) * b.get_unchecked(k*N+j);;
                }
			}
		}
	}
}

fn medir_tiempos(f: fn(&Vec<f64>, &Vec<f64>, &mut Vec<f64>) -> (), a: &Vec<f64>, b: &Vec<f64>, res: &mut Vec<f64>) -> f64 {
    let start = Instant::now();
    f(a, b, res);
    let elapsed = start.elapsed();
    let secs = (elapsed.as_secs() as f64) + (elapsed.subsec_nanos() as f64 / 1000_000_000.0);
    return secs
}

fn realizar_experimento() {
    let mut a = matriz_ceros();
    let mut b = matriz_ceros();
    let mut c = matriz_ceros();
    matriz_aleatoria(&mut a);
    matriz_aleatoria(&mut b);

    println!("{}", medir_tiempos(mult_matrices, &a, &b, &mut c));
}

fn main() {
    realizar_experimento();
}