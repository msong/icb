import numpy as np
import time
import random
import sys

global N
N = 499  # NLINE


def matrizCeros():
    return [[0.0 for _ in range(N)] for _ in range(N)]


def matrizAleatoria(a):
    for i, _ in enumerate(a):
        for j, _ in enumerate(a):
            a[i][j] = random.random()


def multMatrices(a, b, res):
    if isinstance(a, np.matrix):
        np.copyto(res, np.matmul(a, b))
    else:
        for i, row in enumerate(a):
            for j, col in enumerate(zip(*b)):
                res[i][j] = sum(p1 * p2 for p1, p2 in zip(row, col))


def medirTiempos(fn, *args):
    start = time.time()
    fn(*args)
    return time.time() - start


def realizarExperimento():
    a, b, c = matrizCeros(), matrizCeros(), matrizCeros()
    matrizAleatoria(a)
    matrizAleatoria(b)
    elapsed = medirTiempos(multMatrices, a, b, c)
    print("Tiempo total listas: " + str(elapsed))

    c = np.zeros((N, N))
    elapsed = medirTiempos(multMatrices, np.matrix(a), np.matrix(b), c)
    print("Tiempo total numpy: " + str(elapsed))

# Las siguientes funciones son para facilitarme la vida con benchmark.py, y printear solo la duracion. Ver "metodologia".


def realizarExperimentoListas():
    a, b, c = matrizCeros(), matrizCeros(), matrizCeros()
    matrizAleatoria(a)
    matrizAleatoria(b)
    elapsed = medirTiempos(multMatrices, a, b, c)
    print(elapsed)


def realizarExperimentoNumpy():
    a, b = matrizCeros(), matrizCeros()
    matrizAleatoria(a)
    matrizAleatoria(b)
    c = np.zeros((N, N))
    elapsed = medirTiempos(multMatrices, np.matrix(a), np.matrix(b), c)
    print(elapsed)


if __name__ == "__main__":
    _, *flags = sys.argv
    if flags:
        if flags[0] == "numpy":
            realizarExperimentoNumpy()
        if flags[0] == "listas":
            realizarExperimentoListas()
    else:
        realizarExperimento()
