N = 5000 -- NLINE
function matrizCeros()
    m = {}
    for _=1, N do
        r = {}
        for _=1, N do
            table.insert(r, 0)
        end
        table.insert(m, r)
    end
    return m
 end

 function matrizAleatoria(a)
    for i, _ in ipairs(a) do
        for j, _ in ipairs(a) do
            a[i][j] = math.random()
        end
    end
 end

 function multMatrices(a, b, res)
    for x=1, #a do
        for y=1, #b[1] do
            for z=1, #b do
                res[x][y] = res[x][y] + a[x][z] * b[z][y]
            end
        end
    end
end
 

a = matrizCeros()
b = matrizCeros()
r = matrizCeros()
matrizAleatoria(a)
matrizAleatoria(b)
start = os.clock()
multMatrices(a, b, r)
elapsed = os.clock()-start
print(elapsed)