# Lo hago en un archivo aparte porque theano hace cosas raras on-import

import numpy as np
import time
import random
import sys
import theano

global N
N = 2000  # NLINE

x = theano.tensor.matrix()
y = theano.tensor.matrix()
dot = theano.tensor.dot(x, y)

MATMUL_FUNC = theano.function([x, y], [dot])


def matrizCeros():
    return [[0.0 for _ in range(N)] for _ in range(N)]


def matrizAleatoria(a):
    for i, _ in enumerate(a):
        for j, _ in enumerate(a):
            a[i][j] = random.random()


def multMatrices(a, b, res):
    res = MATMUL_FUNC(a, b)


def medirTiempos(fn, *args):
    start = time.time()
    fn(*args)
    return time.time() - start


def realizarExperimento():
    a, b, c = matrizCeros(), matrizCeros(), matrizCeros()
    matrizAleatoria(a)
    matrizAleatoria(b)
    elapsed = medirTiempos(multMatrices, np.matrix(a), np.matrix(b), c)
    print(elapsed)


if __name__ == "__main__":
    realizarExperimento()
