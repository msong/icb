import theano
import numpy as np
import time

x = theano.tensor.matrix()
y = theano.tensor.matrix()
dot = theano.tensor.dot(x, y)

f = theano.function([x, y], [dot])


for n in [100, 500] + list(range(1000, 11000, 1000)):
    a = np.random.random((n, n))
    b = np.random.random((n, n))

    start = time.time()
    res = f(a, b)
    end = time.time() - start
    print(f"Matmul NxN N={n} en {end:.5f} segundos")
