/*
$ node main.js
o abrir browser en `about:blank` y copypastear en la consola.
*/

const N = 5000; // NLINE
const S = N * N;

function matrizCeros() {
    return new Array(S).fill(0);
}

function matrizAleatoria(a) {
    a.forEach(function (_, i) { a[i] = Math.random(); });
}

function multMatrices(a, b, res) {
    for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++) {
            for (k = 0; k < N; k++) {
                res[i * N + j] += a[i * N + k] * b[k * N + j];
            }
        }
    }
}

function medirTiempos(func, a, b, res) {
    var start = Date.now();
    func(a, b, res);
    return (Date.now() - start) / 1000;
}

function realizarExperimento() {
    var a = matrizCeros();
    var b = matrizCeros();
    var c = matrizCeros();

    matrizAleatoria(a);
    matrizAleatoria(b);
    console.log(medirTiempos(multMatrices, a, b, c));
}

realizarExperimento();