from benchmark import Benchmark
import os


RESULTS_DIR = "t_vs_n/"


class BenchmarkN(Benchmark):
    def __init__(self, name, from_n, to_n, source_file, build_cmd, run_cmd):
        self.name = name
        self.from_n = from_n
        self.to_n = to_n
        self.source_file = source_file
        self.build_cmd = build_cmd
        self.run_cmd = run_cmd

    def start(self):
        with open(f"{RESULTS_DIR}{self.name}.csv", "w") as fw:
            for n_matrix in range(self.from_n, self.to_n):
                self.replace_n(n_matrix)
                if self.build_cmd:
                    self.build()

                elapsed = float(self.run().strip())
                print(f"[RUN {100*(n_matrix-self.from_n)/(self.to_n-self.from_n):.2f}%][{self.name} N={n_matrix}] \t {elapsed}")
                fw.write(f"{n_matrix}, {elapsed}\n")


if __name__ == "__main__":
    BenchmarkN("C++ -O3", 100, 500, "code/cpp/main.cpp", "g++ code/cpp/main.cpp -O3 -o code/cpp/main", "code/cpp/main").start()
    BenchmarkN("Go", 100, 500, "code/go/main.go", "go build -o code/go/main code/go/main.go", "code/go/main").start()
    BenchmarkN("Python", 100, 500, "code/python/main.py", "", "python3 code/python/main.py listas").start()
    BenchmarkN("Python NumPy", 100, 500, "code/python/main.py", "", "python3 code/python/main.py numpy").start()
    BenchmarkN("JavaScript", 100, 500, "code/js/main.js", "", "node code/js/main.js").start()
    BenchmarkN("Rust", 100, 500, "code/rust/src/main.rs", "cargo build --manifest-path=code/rust/Cargo.toml --release", "code/rust/target/release/rust").start()
