import time
import random


def esPrimo(x):
    """
    esPrimo recibe un entero x, y devuelve True si su absoluto es primo, False si no lo es.
    """
    x = abs(x)

    i = 1
    divs = 0
    while i <= x and divs < 3:
        if x % i == 0:
            divs += 1
        i += 1
    return True if divs == 2 else False


def Resacon(n, funcPrimo=esPrimo):
    """
    Resacon devuelve el n-ésimo número que cumple tanto que es primo y potencia de 2 menos 1.
    """
    if not (n >= 1):
        raise ValueError("El argumento tiene que ser >= 1.")

    i, j, res = 0, 0, 0
    while i < n:
        j += 1
        res = 2**j-1
        if funcPrimo(res):
            i += 1
    return res

# Here be dragons


def memo(f):
    """
    memo es un decorador.
    memo es un envoltorio que no interfiere con los parámetros de otros, y solo existe para que las llamadas repetidas
    a otras funciones con los mismos parámetros no tengan que calcularse de nuevo, porque guarda los resultados.
    memo es bueno.
    """
    cache = {}

    def hook(x):
        if x not in cache:
            cache[x] = f(x)
        return cache[x]
    return hook


@memo
def esPosiblePrimo(x, k=8):
    """
    Salí de compras por la Wikipedia y me vendieron esto.

    esPosiblePrimo recibe un numero y devuelve True si es un <posible> primo o False si no es primo, según el pequeño teorema de Fermat.
    No tiene garantía, si da True puede no ser un primo verdadero, pero si da False seguro que no es primo.
    Para x < 4 devuelve si es primo o no de verdad, no de mentiritas. Si no, lo hace probando k veces números al azar, y, y...
    Mirá en lo que me metí. Que bien vendría una especificación, pero no tengo espacio suficiente en el margen de la hoja. Que macana.

    https://en.wikipedia.org/wiki/Fermat_primality_test
    """
    if x < 4:
        return esPrimo(x)

    posible = True
    i = 0
    while posible and i < k:
        # (x**y) % z == pow(x, y[, z]), pero es MUCHO más rápido al usar modulo
        r = pow(random.randint(2, x-2), x-1, x)
        if r != 1:
            posible = False
        i += 1
    return posible


if __name__ == '__main__':
    # Acá contribuimos al calentamiento global
    print("Numeros primos y potencia de 2 menos 1:")
    for i in range(1, 8):
        inicio = time.time()
        print(f"Resacon #{i}: {Resacon(i)} (en {time.time()-inicio:.8f} segs)")

    print("En el tiempo que te ponés a esperar a que salga el 8vo, esos randoms de la internet ya te encontraron 50 primos más.")
    print("Y no porque tu tio sea Maradona.\n")

    print("Que papelón, todavía no salimos de la prehistoria. Ahora me quedé manija.\n")
    input("Presiona la tecla cualquiera...")

    for i in range(1, 20):
        inicio = time.time()
        print(f"Resacon Violento #{i}: {Resacon(i, funcPrimo=esPosiblePrimo)} (en {time.time()-inicio:.8f} segs)")

    # Lo razonable seria hacer esPosiblePrimo AND esPrimo, con la baratija a la izquierda como optimización,
    # pero asi a lo bruto parece cumplirse bastante bien:
    # https://en.wikipedia.org/wiki/Mersenne_prime#List_of_known_Mersenne_primes
