import unittest
from Taller1.main import esPrimo, esPosiblePrimo, Resacon


class TestEsPrimo(unittest.TestCase):
    def test_edges(self):
        self.assertTrue(esPrimo(-2))
        self.assertFalse(esPrimo(-1))
        self.assertFalse(esPrimo(0))
        self.assertFalse(esPrimo(1))
        self.assertTrue(esPrimo(2))

    def test_numeritos(self):
        self.assertTrue(esPrimo(3))
        self.assertFalse(esPrimo(4))
        self.assertTrue(esPrimo(-7561))
        self.assertTrue(esPrimo(7561))


class TestPosiblePrimo(unittest.TestCase):
    def test_numeritos(self):
        self.assertFalse(esPosiblePrimo(117))
        self.assertFalse(esPosiblePrimo(77))
        self.assertFalse(esPosiblePrimo(159))
        self.assertFalse(esPosiblePrimo(451))
        self.assertFalse(esPosiblePrimo(76789547))


class TestResacon(unittest.TestCase):
    def test_edges(self):
        with self.assertRaises(ValueError):
            Resacon(0)

    def test_numeritos(self):
        self.assertEqual(Resacon(1), 3)
        self.assertEqual(Resacon(2), 7)
        self.assertEqual(Resacon(3), 31)
        self.assertEqual(Resacon(4), 127)
