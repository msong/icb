# Ejercicio 2
# Especifique e implemente las siguientes funciones booleanas:


def noEsCero(n):
    return n != 0


def iguales(n1, n2):
    return n1 == n2


def menor(n1, n2):
    return n1 < n2


def par(n):
    return n % 2 == 0


def divisible(n, d):
    return n % d == 0


def imparDivisiblePorTresOCinco(n):
    return (n % 3 == 0 or n % 5 == 0) and n % 2 != 0
