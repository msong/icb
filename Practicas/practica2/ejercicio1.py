# chirimbolaje: ℝ ℤ → ∧∨¬ ⊥ ≠ ∃ ∀ β ≤ Σ

# Ejercicio 1
# Especifique e implemente las siguientes funciones:


def doble(n):
    return n * 2


def signo(n):
    if n < 0:
        return -1
    if n == 0:
        return 0
    if n > 0:
        return 1


def abs(n):
    return -1 * n if n < 0 else n


def inversoMultiplicativo(n):
    return 1 / n


def suma3(n1, n2, n3):
    return n1 + n2 + n3


def promedio3(n1, n2, n3):
    return (n1 + n2 + n3) / 3


def maximo3(n1, n2, n3):
    if n1 >= n2 and n1 >= n3:
        return n1
    if n2 >= n1 and n2 >= n3:
        return n2
    if n3 >= n1 and n3 >= n2:
        return n3


def maximoAbsoluto3(n1, n2, n3):
    return maximo3(abs(n1), abs(n2), abs(n3))
