# Ejercicio 3
# Especifique e implemente las siguientes funciones sobre enteros:


def factorial(n):
    if n > 1:
        return n * factorial(n - 1)
    else:
        return 1


def sumaDivisores(n):
    suma = 0
    for x in range(1, n + 1):
        if n % x == 0:
            suma += x
    return suma


def primo(n):
    for x in range(2, n):
        if n % x == 0:
            return False
    return True


def menorDivisiblePorTres(n):
    while n % 3 != 0:
        n += 1
    return n


def mayorPrimo(n1, n2):
    if not n1 < n2:
        raise ValueError("n1 tiene que ser menor a n2")
    if not primo(n1) or n2 % n1 != 0:
        return False
    for i in range(n1 + 1, n2):
        if primo(i) and n2 % i == 0:
            return False
    return True


assert mayorPrimo(11, 22)


def potencia(n1, n2):
    last = 0
    exp = 0
    while last < n1:
        last = n2**exp
        exp += 1
    return last == n1


assert potencia(128, 2)
assert not potencia(65, 2)


def mcd(n1, n2):
    for i in range(n1, 0, -1):
        if n1 % i == 0 and n2 % i == 0:
            return i


assert mcd(10, 100) == 10
assert mcd(12, 8) == 4
