def A(n):
    return n**2


def B(a):
    # Sumar los que tienen indice divisible por 2
    suma = 0
    for i, v in enumerate(a):
        if i % 2 == 0:
            suma += v
    return suma


assert B([1, 2, 3, 4]) == 4


def C(a):
    # Capicua
    c = True
    i = 0
    while c and i < len(a):
        c = a[i] == a[len(a) - 1 - i]
        i += 1
    return c


assert C([1, 2, 2, 1])
assert C([1, 2, 2, 2, 1])
assert not C([1, 2, 3, 4])
assert C([5, 5, 9, 9, 5, 5])
assert C([])


def D(a):
    suma = 0
    for i, v in enumerate(a):
        if i % 2 == 1:
            suma += v
    return suma / (len(a) / 2)


def E(a):
    # Minimo
    min_v = a[0]

    for v in a:
        if v <= min_v:
            min_v = v
    return min_v


assert E([6, 3, 129, 2, 4]) == 2


def F(a):
    """
    Diferencia indice_fin-indice_inicio del grupo consecutivo que posee mayor longitud.
    ej: [1,2,2,2,3,4,4,1]: por indices, [4,4]: 6-5=1, [2,2,2]: 3-1 = 2. 

    NOTE: me parece que le falta un requiere |a| > 0 o especificar como tiene que ser
          res si se recibe una lista vacia.
    """
    i_inicio, i_fin, val = 0, 0, a[0]
    grupo_max_i_diff = 0
    for i, x in enumerate(a):
        if x != val:
            if i_fin - i_inicio > grupo_max_i_diff:
                grupo_max_i_diff = i_fin - i_inicio
            val, i_inicio, i_fin = x, i, i
        else:
            i_fin = i

    # No me gusta esto repetido, pero bue
    if i_fin - i_inicio > grupo_max_i_diff:
        grupo_max_i_diff = i_fin - i_inicio

    return grupo_max_i_diff


assert F([1, 2, 2, 2, 3, 4, 4, 1]) == 2
assert F([1]) == F([80]) == 0
assert F([1, 2, 2]) == 1
assert F([1, 3, 3, 0, 1, 1, 9, 9, 9, 9, 1]) == 3
