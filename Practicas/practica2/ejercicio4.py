# Ejercicio 4
# Especifique e implemente las siguientes funciones sobre secuencias. Para la implementaci´on, en los
# casos en los que exista una funci´on equivalente en Python, no est´a permitido utilizarla.


def suma(a):
    s = 0
    for x in a:
        s += x
    return s


def promedio(a):
    suma = 0
    for x in a:
        suma += x
    return suma / len(a)


def maximo(a):
    max = a[0]
    for x in a:
        if abs(x) > max:
            max = abs(x)
    return max


def listaDeAbs(a):
    return [abs(x) for x in a]


def maximoAbsoluto(a):
    max_abs = a[0]
    for x in a:
        if abs(x) > max_abs:
            max_abs = abs(x)
    return max_abs


assert maximoAbsoluto([12, -99, 1928, 3]) == 99


def divisores(n):
    divs = []
    for k in range(len(n)):
        if n % k == 0:
            divs.append(k)
    return divs


assert divisores(10) == [1, 2, 5, 10]


def cantidadApariciones(a, x):
    c = 0
    for e in a:
        if e == x:
            c += 1
    return c


assert cantidadApariciones(["a", "x", "asd", "x"], "x") == 2


def masRepetido(a):
    c = {k: 0 for k in a}
    for e in a:
        c[e] += 1
    k_max, v_max = None, 0
    for k, v in c.items():
        if v > v_max:
            k_max, v_max = k, v
    return k_max


assert masRepetido([2, 1, 3, 1]) == 1
assert masRepetido(["a", "b", "c", "b"]) == "b"
assert masRepetido([(0, 1), (0, 0), (0, 1)]) == (0, 1)


def todosPares(a):
    p = True
    for x in a:
        if x % 2 != 0:
            p = False
    return p


assert todosPares([6, 12, 98])
assert not todosPares([22, 38, 101, 96])


def ordenAscendente(a):
    asc = True
    i = 0
    while asc and i < len(a) - 1:
        if a[i] > a[i + 1]:
            asc = False
        i += 1
    return asc


assert ordenAscendente([])
assert ordenAscendente([589, 666, 829, 999])
assert not ordenAscendente([589, 12, 829, 999])


def reverso(a):
    return a[::-1]


def reverso_posta(a):
    return [a[i] for i in range(len(a) - 1, -1, -1)]


# Bueno che esta bien, lo hacemos en serio
def reverso_posta_posta(a):
    rev = []
    for x in a:
        rev.insert(0, x)
    return rev


assert reverso_posta_posta(["a", "zz", "e"]) == ["e", "zz", "a"]
