def fact(n):
    if n == 0:
        return 1
    else:
        return n * fact(n - 1)


def fib(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fib(n - 1) + fib(n - 2)


def sumaPotenciasDeDos(n1, n2):
    if n1 == n2:
        return 2**n2
    else:
        return 2**n2 + sumaPotenciasDeDos(n1, n2 - 1)


assert sumaPotenciasDeDos(5, 8) == 2**5 + 2**6 + 2**7 + 2**8


def sumaImpares(n):
    if n == 1:
        return n if n % 2 != 0 else 0
    else:
        return (n if n % 2 != 0 else 0) + sumaImpares(n - 1)


assert sumaImpares(10) == 1 + 3 + 5 + 7 + 9


def sumaDigitos(n):
    if n == 0:
        return n % 10
    else:
        return n % 10 + sumaDigitos(n // 10)


assert sumaDigitos(5912) == 5 + 9 + 1 + 2


def sumaDivisores(n, d=1):
    if d == n:
        return n
    else:
        return (d if n % d == 0 else 0) + sumaDivisores(n, d + 1)


assert sumaDivisores(10) == 1 + 2 + 5 + 10


def divisiblePor3(n):
    if n < 10:
        return True if n in (3, 6, 9) else False
    else:
        return divisiblePor3(sumaDigitos(n))


assert divisiblePor3(3 * 467867)


def divisiblePor17(n):
    if n == 17 or n == 0:
        return True
    if n < 17:
        return False
    else:
        return divisiblePor17(abs(n // 10 - 5 * (n % 10)))


assert divisiblePor17(102)


def suma(a):
    if len(a) == 0:
        return 0
    else:
        return a[0] + suma(a[1:])


assert suma([1, 2, 3]) == 6


def maximo(a):
    if len(a) == 1:
        return a[0]
    else:
        if a[0] < a[1]:
            return maximo(a[1:])
        else:
            return maximo(a[:1] + a[2:])


assert maximo([1, 989, 1, 2, 19823, 1]) == 19823


def promedio(l):
    def suma(a):
        if len(a) == 0:
            return 0
        else:
            return a[0] + suma(a[1:])
    return suma(l) / len(l)


assert promedio([1, 2, 3, 4]) == 2.5


def maximoAbsoluto(a):
    if len(a) == 0:
        return 0
    else:
        max_resto = maximoAbsoluto(a[1:])
        return a[0] if a[0] >= max_resto else max_resto


assert maximoAbsoluto([6, 2, 1, 923, 1]) == 923


def listaDeAbs(a):
    if len(a) == 0:
        return []
    else:
        return [abs(a[0])] + listaDeAbs(a[1:])


assert listaDeAbs([-1, 2, 3, -42, -2]) == [1, 2, 3, 42, 2]


def cambioDeBase(n, base):
    if n == 0:
        return []
    else:
        return cambioDeBase(n // base, base) + [n % base]


assert cambioDeBase(16, 2) == [1, 0, 0, 0, 0]


def cantidadApariciones(a, x):
    if len(a) == 0:
        return 0
    else:
        return cantidadApariciones(a[1:], x) + (1 if a[0] == x else 0)


assert cantidadApariciones([1, 2, 3, 42, 42, 4, 2, 42], 42) == 3


def eliminar(a, i):
    if len(a) == 0:
        return []
    else:
        return ([a[0]] if i != 0 else []) + eliminar(a[1:], i - 1)


assert eliminar([1, 2, 3, 4], 3) == [1, 2, 3]


def buscarYEliminar(a, x):
    if len(a) == 0:
        return []
    else:
        return ([a[0]] if a[0] != x else []) + buscarYEliminar(a[1:], x)


assert buscarYEliminar(["asd", "eaea", "ok"], "eaea") == ["asd", "ok"]


def todosPares(a):
    if len(a) == 0:
        return True
    else:
        return (a[0] % 2 == 0) and todosPares(a[1:])


assert todosPares([2, 92, 60, 4])
assert not todosPares([2, 92, 7, 60, 4])


def ordenAscendente(a):
    if len(a) < 2:
        return True
    elif len(a) == 2:
        return (a[0] < a[1])
    else:
        return (a[0] < a[1]) and ordenAscendente(a[1:])


assert ordenAscendente([94, 95, 96, 910])
assert not ordenAscendente([94, 95, 94, 96, 910])


def reverso(a):
    if len(a) == 1:
        return [a[0]]
    else:
        return reverso(a[1:]) + [a[0]]


assert reverso([1, 2, 3]) == [3, 2, 1]
assert not reverso([1, 4, 2, 3]) == [3, 4, 2, 1]


def sumaPosImpares(a):
    if len(a) == 0:
        return 0
    if len(a) <= 2:
        return a[0]
    else:
        return a[0] + sumaPosImpares(a[2:])


assert sumaPosImpares([4, 5, 12, 2, 41, 2]) == 4 + 12 + 41


# def triangular(a):
#     if len(a) < 2:
#         return True
#     if len(a) == 2:
#         return a[0] <= a[1]
#     else:
#         return (a[0] <= a[1]) and (a[-2] >= a[-1]) and triangular(a[1:-1])

def triangular(a, crec=True):
    if len(a) < 2:
        return True
    if (a[0] > a[1]) and crec:
        return True and triangular(a[1:], False)
    if len(a) == 2:
        return a[0] <= a[1] if crec else a[0] >= a[1]
    else:
        if crec:
            return (a[0] <= a[1]) and triangular(a[1:], crec)
        else:
            return (a[0] >= a[1]) and triangular(a[1:], crec)


assert triangular([1, 2, 3, 4, 3, 2, 1])
assert triangular([1, 2, 3, 4, 5, 6, 1])
assert triangular([1, 2, 3, 80, 8, 1])
assert triangular([1, 2, 80, 8, 1])
assert triangular([1, 2, 3, 3, 3])
assert not triangular([1, 2, 3, 80, 7, 8, 1])
assert not triangular([1, 2, 1, 2])
assert triangular([1])
assert triangular([])


def map(fn, a):
    if len(a) == 0:
        return []
    else:
        return [fn(a[0])] + map(fn, a[1:])


def filter(fn, a):
    if len(a) == 0:
        return []
    else:
        return ([a[0]] if fn(a[0]) else []) + filter(fn, a[1:])


def reduce(fn, a):
    if len(a) == 1:
        return a[0]
    else:
        return fn(a[0], reduce(fn, a[1:]))

# d) (Opcional) Piense c´omo implementar´ıa las funciones suma, maximo, cantidadApariciones,
# todosPares y ordenAscendente del ejercicio 2 utilizando alguna combinaci´on de los esquemas
# de recursi´on presentados en los ´ıtems anteriores.


suma = lambda a: reduce(lambda x1, x2: x1 + x2, a)
assert suma([1, 2, 3, 4, 5]) == 15

maximo = lambda a: reduce(lambda x1, x2: x1 if x1 > x2 else x2, a)
assert maximo([1, 81, 2, 3, 90, 12]) == 90

cantidadApariciones = lambda a, x: reduce(lambda x1, x2: x1 + x2, map(lambda ap: 1 if ap == x else 0, a))
assert cantidadApariciones([1, 2, 3, 4, 2, 5, 2], 2) == 3


todosPares = lambda a: reduce(lambda x1, x2: x1 and x2, map(lambda x: x % 2 == 0, a))
assert todosPares([2, 4, 8, 11212])
assert not todosPares([2, 4, 8, 1, 11212])

# ordenAscendente = lambda a: reduce(lambda x1, x2: , a)
# TODO: No se, probe de mil formas con reduce (map y filter no creo que sirvan aca)


def A(a, j):
    if j == 0:
        return abs(a[j] * a[j]**j)
    else:
        return abs(a[j] * a[j]**j) + A(a, j - 1)


assert A([1, 2, 3, 4], 2) == 32


def esPrimo(k, d=2):
    if k < 2:
        return False
    if k == 2:
        return True
    if d == k:
        return True
    else:
        return k % d != 0 and esPrimo(k, d + 1)


def B(n1, n2):
    if esPrimo(n1):
        return n1
    else:
        return B(n1 + 1, n2)


assert B(20, 100) == 23
assert B(8, 22) == 11


def C(A, B):
    def C2(A, B, i=0):
        if len(A) == 0:
            return []
        else:
            return [A[0] + B[i % len(B)]] + C2(A[1:], B, i + 1)
    return C2(A, B)


assert C([1, 2, 3], [3, 2, 1]) == [4, 4, 4]
assert C([], [1, 2]) == []


def D(X, V):
    if len(X) == 1:
        return [X[0]] if X[0] not in V else []
    else:
        return ([X[0]] + D(X[1:], V)) if X[0] not in V else D(X[1:], V)


assert D([1, 2, 3], [2, 4, 5, 2, 6, 2, 6]) == [1, 3]
